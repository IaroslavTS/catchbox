//let fruits = ['orange', 'apple', 'banana'];

//let deleted = fruits.pop();
//console.log(fruits, deleted);

//fruits.push('mango');
//console.log(fruits);

//const deleted = fruits.shift();
//console.log(fruits, deleted);

//fruits.unshift('strawberrys');
//console.log(fruits);

//let arr = [1, 2, 3];
//let arr1 = [12, 23, 34];
//let newArr=fruits.concat(arr, arr1);
//console.log(newArr);

//slece копіює стрічку з того моменту який був вказаний в дужках
//let arr=fruits.slice(1, 2);
//console.log(arr);

//fruits.splice(1, 0, 'mango');
//console.log(fruits);

//console.log(fruits.join('-'));

//let str = fruits.join(' ');
//let arr = str.split(' ');
//console.log(str,arr);

//let newArr = fruits.reverse();
//console.log(newArr);

//function customSort(a, b) {
//    if (a > b) return 1;
//    if (a < b) return -1;
//}
//const numbers = [1, 18, 2, 123];
//const sortElem = numbers.sort(customSort);
//console.log(sortElem);

//const name= 'apple';
//let a = fruits.indexOf(name);
//console.log(a);

//const numbers = [1, 18, 2, 123, 1, 5, 6, 1],
//    n = 1;
//let start = 0,
//    one = [];
//
//while (true) {
//    const item = numbers.indexOf(n, start);
//    if (item === -1) {
//        break;
//    }
//    one.push(numbers[item])
//    start = item + 1;
//}
//console.log(one);

//let newArr = [];
//fruits.forEach(function(value){ //function(value, index, arr) перебирвє
//    newArr.push(`The ${value}`);
//});
//console.log(newArr);
//
//let users = [
//    {name: 'Ivan', status: true},
//    {name: 'Petro', status: false},
//    {name: 'Tanya', status: true},
//    {name: 'Ivanka', status: false},
//    {name: 'Katya', status: true},
//    {name: 'Yura', status: false}
//];
//
//let filteredUsers = users.filter(function(value) { // Filtered function(value, index, arr)
//   if (value.status) {
//       return value;
//   } 
//});
//console.log(filteredUsers);
//
////map
//let count = users.map(function(value){
//    return value.name.length;
//});
//
//console.log(count);

//методи стрічок
//length
//var s = 'Hello';
//s.length = 2;
//console.log(s.length, s);
//console.log(users.length);
//
//users.length = 2; //можна змінити
//console.log(users.length, users);

//charAt
//console.log(s.charAt('1'));

////toUpperCase toLowerCase
//console.log(s.toUpperCase()); //літери всі у верхній регістр
//console.log(s.toLowerCase()); //літери всі у нижній регістр

//substring
//var s = "Javascript"
//console.log(s.substring(4, 9));

//substr
//console.log(s.substr(4, 3));

//slice
//console.log(s.slice(3, -4));
let countPairs,
    coordX = [],
    coordY = [],
    count = 0;
const box = getE('.box'),
    colors = getE('.colors').children;

function getE(element) {
    return document.querySelector(element);
}

let numberIs = function (number) {
    return !isNaN(parseInt(number)) && number > 0;
}

getE('.create-element').addEventListener('click', function () {
    box.style.display = 'block';
});

getE('.box-width').addEventListener('keyup', function () {
    let boxWidth = getE('.box-width').value;
    box.style.width = `${boxWidth}px`;
});

getE('.box-height').addEventListener('keyup', function () {
    let boxHeight = getE('.box-height').value;
    box.style.height = `${boxHeight}px`;
});


for (let i = 0; i < colors.length; i++) {
    colors[i].addEventListener('click', function () {
        let color = colors[i].className;
        box.className = `box ${color}`;
    });
}

getE('.coordinates').addEventListener('click', function () {
    countPairs = parseInt(prompt('Enter the number of pairs'));
    while (!numberIs(countPairs)) {
        countPairs = parseInt(prompt('Enter the number of pairs'));
    }
    for (let i = 0; i < 2 * countPairs; i++) {
        if (i < countPairs) {
            coordX[i] = parseInt(prompt('Enter coordinate X'));
        } else {
            coordY[i-countPairs] = parseInt(prompt('Enter coordinate Y'));
        }
    }
});
//debugger
//for (let i = 0; i < countPairs; i++) {
box.addEventListener('mouseover', function(){
    if (count < countPairs) {
        this.style.left = `${coordX[count]}px`;
        this.style.top = `${coordY[count]}px`;
        count++;
    } else {
        count = 0;
        this.style.left = `0`;
        this.style.top = '0';
    }
});
//}
